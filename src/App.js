import React from "react";
import "./Components/styles.css";

import Navbar from "./Components/Navbar";
import MainSection from "./Components/MainSection";
import SecondSection from "./Components/SecondSection";
import Footer from "./Components/Footer";

function App() {
  return (
    <div className="App">
      <Navbar></Navbar>
      <MainSection></MainSection>
      <SecondSection></SecondSection>
      <Footer></Footer>
    </div>
  );
}

export default App;
