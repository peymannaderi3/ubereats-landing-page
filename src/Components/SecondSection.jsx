import React from "react";

import CarouselList from "./CarouselList.jsx";
import MiddleCards from "./MiddleCards";
import Cities from "./Cities";
import Categories from "./Categories";

export default class SecondSection extends React.Component {
  render() {
    return (
      <div
        className="wrapper"
        style={{
          background: "white",
          alignItems: "center",
          margin: "auto",
          marginTop: 40
        }}
      >
        <CarouselList title="Food Delivery in San Francisco Bay Area"></CarouselList>
        <hr style={{ marginTop: 40, marginBottom: 40 }}></hr>
        <MiddleCards></MiddleCards>
        <hr style={{ marginTop: 40, marginBottom: 40 }}></hr>
        <CarouselList title="New on Uber Eats"></CarouselList>
        <hr style={{ marginTop: 40, marginBottom: 40 }}></hr>
        <Cities></Cities>
        <hr style={{ marginTop: 40, marginBottom: 40 }}></hr>
        <Categories></Categories>
      </div>
    );
  }
}
