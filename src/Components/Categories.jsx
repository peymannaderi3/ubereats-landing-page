import React from "react";
import burger from "../Assets/burger.jpg";
const category = [
  "American",
  "Burgers",
  "Desserts",
  "Fast Food",
  "Mexican",
  "Pizza"
];
export default class Categories extends React.Component {
  render() {
    return (
      <div
        style={{
          width: "100%"
        }}
      >
        <div
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "space-between"
          }}
        >
          <h3
            style={{
              lineHeight: "32px",
              fontSize: 26,
              color: "#1F1F1F",
              margin: "none",
              fontWeight: "normal",
              display: "inline-block",
              margin: 0,
              textAlign: "left"
            }}
          >
            Explore Popular Categories
          </h3>
        </div>
        <div
          style={{
            width: "100%",
            marginTop: 24,
            display: "flex",
            justifyContent: "space-between",
            overflow: "auto"
          }}
        >
          {category.map((item, i) => {
            return (
              <a
                key={"category-" + i}
                style={{
                  padding: 10,
                  textAlign: "left",
                  fontSize: 16,
                  color: "#1F1F1F",
                  cursor: "pointer",
                  display: "inline-flex",
                  flexDirection: "column"
                }}
              >
                <img
                  src={burger}
                  style={{
                    width: 110,
                    height: 110,
                    borderRadius: 55,
                    overflow: "hidden",
                    marginBottom: 8
                  }}
                ></img>
                <p style={{ margin: 0, textAlign: "center" }}>{item} </p>
              </a>
            );
          })}
        </div>
      </div>
    );
  }
}
