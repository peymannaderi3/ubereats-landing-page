import React from "react";

import card1 from "../Assets/middleCard1.svg";
import card2 from "../Assets/middleCard2.svg";
import card3 from "../Assets/middleCard3.svg";
import googlePlay from "../Assets/googlePlay.svg";
import apple from "../Assets/apple.svg";
export default class MiddleCards extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div
        className="middleCardsParent"
        style={{
          width: "100%",
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          marginBottom: 20
        }}
      >
        <div className="middleCards" style={{ width: "calc(33% - 10px)" }}>
          <div style={{ height: 200, marginBottom: 16 }}>
            <img
              src={card1}
              style={{
                width: "100%",
                height: "100%",
                objectFit: "cover"
              }}
            ></img>
          </div>
          <p
            style={{
              lineHeight: "32px",
              fontSize: 26,
              letterSpacing: "0.03em",
              textAlign: "left",
              margin: 0,
              fontWeight: "normal"
            }}
          >
            There's more to love in the app.
          </p>
          <div
            style={{
              width: "100%",
              display: "flex",
              justifyContent: "space-between",
              marginTop: 8
            }}
          >
            <div style={{ lineHeight: "24px", fontSize: 16, color: "#1F1F1F" }}>
              Get the app.
            </div>
            <div
              style={{
                lineHeight: "24px",
                fontSize: 16,
                color: "#1F1F1F",
                display: "flex",
                alignItems: "center",
                cursor: "pointer"
              }}
            >
              <img
                src={apple}
                style={{ width: 16, height: 19, marginRight: 10 }}
              ></img>
              iPhone
            </div>
            <div
              style={{
                lineHeight: "24px",
                fontSize: 16,
                color: "#1F1F1F",
                display: "flex",
                alignItems: "center",
                cursor: "pointer"
              }}
            >
              <img
                src={googlePlay}
                style={{
                  width: 16,
                  height: 19,
                  marginRight: 10,
                  marginLeft: 10
                }}
              ></img>
              Android
            </div>
          </div>
        </div>
        <div
          className="middleCards"
          style={{ width: "calc(33% - 10px)", textAlign: "left" }}
        >
          <div style={{ height: 200, marginBottom: 16 }}>
            <img
              src={card2}
              style={{
                width: "100%",
                height: "100%",
                marginBottom: 16,
                objectFit: "cover"
              }}
            ></img>
          </div>
          <p
            style={{
              lineHeight: "32px",
              fontSize: 26,
              letterSpacing: "0.03em",
              textAlign: "left",
              margin: 0,
              fontWeight: "normal"
            }}
          >
            Your restaurant, delivered
          </p>
          <a className="middleLink">Add your restaurant</a>
        </div>
        <div
          className="middleCards"
          style={{ width: "calc(33% - 10px)", textAlign: "left" }}
        >
          <div style={{ height: 200, marginBottom: 16 }}>
            <img
              src={card3}
              style={{
                width: "100%",
                height: "100%",
                marginBottom: 16,
                objectFit: "cover"
              }}
            ></img>
          </div>
          <p
            style={{
              lineHeight: "32px",
              fontSize: 26,
              letterSpacing: "0.03em",
              textAlign: "left",
              margin: 0,
              fontWeight: "normal"
            }}
          >
            Deliver the Eats
          </p>
          <a className="middleLink">Sign up to deliver</a>
        </div>
      </div>
    );
  }
}
