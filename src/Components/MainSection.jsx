import React from "react";
import Search from "./Search";
//asset
import sec1a from "../Assets/sec1-pic1.svg";
import sec1b from "../Assets/sec1-pic2.svg";
export default class MainSection extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    window.addEventListener("scroll", function(e) {
      if (window.scrollY == 0) {
        document.getElementById("stickyNav").classList.remove("navBarBox");
      } else {
        document.getElementById("stickyNav").classList.add("navBarBox");
      }
      if (window.scrollY > 300) {
        document
          .getElementById("topInput-top")
          .classList.remove("notShowInput");
        document.getElementById("topInput-top").classList.add("showInput");
        if (window.innerWidth <= 600)
          document.getElementById("secondNav").classList.add("showSecondNav");
      } else {
        document.getElementById("topInput-top").classList.add("notShowInput");
        document.getElementById("topInput-top").classList.remove("showInput");
        if (window.innerWidth <= 600)
          document
            .getElementById("secondNav")
            .classList.remove("showSecondNav");
      }
    });
  }
  componentWillUnmount() {
    window.removeEventListener("scroll");
  }
  render() {
    return (
      <div
        style={{
          background: "#EADFFF",
          height: 415,
          overflow: "hidden",
          width: "100%",
          position: "relative",
          zIndex: 1,
          display: "flex",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <img
          src={sec1a}
          style={{
            height: "100%",
            position: "absolute",
            left: 0,
            zIndex: -1,
            maxWidth: "100%"
          }}
        ></img>
        <img
          src={sec1b}
          style={{
            height: "100%",
            position: "absolute",
            right: 0,
            zIndex: -1,
            maxWidth: "100%"
          }}
        ></img>
        <div
          style={{
            width: 630
          }}
        >
          <h1
            style={{
              marginBottom: 4,
              lineHeight: "44px",
              textAlign: "center",
              fontSize: 36,
              color: "#1F1F1F",
              fontWeight: "normal"
            }}
          >
            Restaurants you love, delivered to you
          </h1>
          <div
            className="mainSmParent"
            style={{
              width: "100%",
              display: "flex",
              marginTop: 40,
              justifyContent: "space-around"
            }}
          >
            <div
              className="mainSm"
              style={{ width: "calc(100% - 130px)", display: "inline-block" }}
            >
              <Search bgColor="white" width="100%" id="middleInput"></Search>
            </div>
            <button
              className="findButton mainSm"
              style={{
                background: "#3E9920",
                color: "white",
                padding: "12px 16px",
                lineHeight: "24px",
                fontSize: 16,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              Find Food
            </button>
          </div>
        </div>
      </div>
    );
  }
}
