import React from "react";

const cites = [
  "New York",
  "Buffalo",
  "Rochester",
  "Yonkers",
  "Syracuse",
  "Albany",
  "New Rochelle",
  "Mount Vernon",
  "Schenectady",
  "Utica",
  "White Plains",
  "Hempstead",
  "Troy",
  "Niagara Falls",
  "Binghamton",
  "Freeport"
];
export default class Cities extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div
        style={{
          width: "100%"
        }}
      >
        <div
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "space-between"
          }}
        >
          <h3
            style={{
              lineHeight: "32px",
              fontSize: 26,
              color: "#1F1F1F",
              margin: "none",
              fontWeight: "normal",
              display: "inline-block",
              margin: 0
            }}
          >
            Cities Near You
          </h3>
          <a className="middleLink">View all 500+ cities</a>
        </div>
        <div
          className="gridCities"
          style={{
            width: "100%",
            marginTop: 24,
            display: "grid"
          }}
        >
          {cites.map((item, i) => {
            return (
              <a
                key={"city-" + i}
                style={{
                  padding: 10,
                  textAlign: "left",
                  fontSize: 16,
                  color: "#1F1F1F",
                  cursor: "pointer"
                }}
              >
                {item}
              </a>
            );
          })}
        </div>
      </div>
    );
  }
}
