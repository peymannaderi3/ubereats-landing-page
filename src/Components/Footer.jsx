import React from "react";

import ubereatsLogo from "../Assets/uberEatsLogo2.svg";
import appstore from "../Assets/appstore.svg";
import googlePlay from "../Assets/googlePlay.png";
import facebook from "../Assets/facebook.svg";
import twitter from "../Assets/twitter.svg";
import instagram from "../Assets/instagram.svg";

const Links = [
  "About Uber Eats",
  "Get Help",
  "Read our blog",
  "Read FAQs",
  "Buy gift cards",
  "View all cities",
  "Sign up to deliver",
  "English",
  "Add your restaurant"
];
export default class Footer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div
        style={{
          alignItems: "center",
          marginTop: 80,
          padding: "72px 0px 88px",
          backgroundColor: "#1F1F1F"
        }}
      >
        <div
          className="wrapper"
          style={{
            margin: "auto",
            display: "flex",
            justifyContent: "space-between"
          }}
        >
          <div
            style={{
              flex: 1,
              textAlign: "left",
              display: "felx",
              justifyContent: "flex-start",
              flexDirection: "row"
            }}
          >
            <img src={ubereatsLogo} style={{ marginBottom: 64 }}></img>
            <div>
              <img src={appstore} style={{ width: 135, margin: 5 }}></img>
              <img src={googlePlay} style={{ width: 135, margin: 5 }}></img>
            </div>
          </div>
          <div style={{ flex: 1, textAlign: "left" }}>
            <div
              style={{
                width: "100%",
                display: "grid",
                gridTemplateColumns: "auto auto"
              }}
            >
              {Links.map((item, i) => {
                return (
                  <a
                    key={"link-" + i}
                    className="footerLink"
                    style={{
                      marginBottom: i < 2 ? 24 : 16,
                      marginBottom: i == Links.length - 1 && i > 2 ? 0 : 16,
                      textAlign: "left",
                      fontSize: 16,
                      color: "white",
                      cursor: "pointer"
                    }}
                  >
                    {item}
                  </a>
                );
              })}
            </div>
          </div>
        </div>
        <hr
          className="wrapper"
          style={{ marginTop: 40, marginBottom: 40 }}
        ></hr>
        <div
          className="wrapper"
          style={{
            margin: "auto",
            display: "flex",
            justifyContent: "space-between"
          }}
        >
          <div
            style={{
              flex: 1,
              textAlign: "left",
              color: "white"
            }}
          >
            © 2019 Uber Technologies Inc.
          </div>
          <div
            style={{
              flex: 1,
              textAlign: "left",
              color: "white",
              display: "flex",
              flexDirection: "column"
            }}
          >
            <div
              style={{
                justifyContent: "space-between",
                display: "flex",
                maxWidth: 350,
                marginBottom: 40
              }}
            >
              <a
                className="footerLink"
                style={{
                  textAlign: "left",
                  fontSize: 16,
                  color: "white",
                  cursor: "pointer"
                }}
              >
                Privacy Policy
              </a>
              <a
                className="footerLink"
                style={{
                  textAlign: "left",
                  fontSize: 16,
                  color: "white",
                  cursor: "pointer"
                }}
              >
                Terms of Use
              </a>
              <a
                className="footerLink"
                style={{
                  textAlign: "left",
                  fontSize: 16,
                  color: "white",
                  cursor: "pointer"
                }}
              >
                Pricing
              </a>
            </div>

            <div
              style={{
                justifyContent: "space-between",
                display: "flex",
                maxWidth: 95
              }}
            >
              <a
                className="footerLink"
                style={{
                  textAlign: "left",
                  fontSize: 16,
                  color: "white",
                  cursor: "pointer"
                }}
              >
                <img src={facebook} style={{ width: 16, height: 16 }}></img>
              </a>
              <a
                className="footerLink"
                style={{
                  textAlign: "left",
                  fontSize: 16,
                  color: "white",
                  cursor: "pointer"
                }}
              >
                <img src={twitter} style={{ width: 16, height: 16 }}></img>
              </a>
              <a
                className="footerLink"
                style={{
                  textAlign: "left",
                  fontSize: 16,
                  color: "white",
                  cursor: "pointer"
                }}
              >
                <img src={instagram} style={{ width: 16, height: 16 }}></img>
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
