import React from "react";
import location from "../Assets/location.svg";

export default class Search extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { bgColor, width, id } = this.props;
    return (
      <div
        className="notShowSm"
        style={{
          width: "100%",
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          height: 48
        }}
      >
        <div
          className="boxShadowTrans"
          id={id}
          style={{
            width: width,
            background: bgColor,
            padding: "12px 16px",
            display: "flex",
            color: "#1F1F1F",
            boxShadow: "inset 0px -1px 0px #E0E0E0",
            position: "relative"
          }}
        >
          <img src={location} style={{ height: 20, marginTop: 1 }}></img>
          <input
            style={{
              padding: 0,
              border: 0,
              background: bgColor,
              flex: 1,
              marginLeft: 15,
              fontSize: 16
            }}
            onFocus={() => {
              document.getElementById(id).classList.add("inputFocus");
            }}
            onBlur={() => {
              document.getElementById(id).classList.remove("inputFocus");
            }}
            placeholder="Enter delivery address"
          ></input>
          <div
            id={id + "-top"}
            className={
              id === "topInput"
                ? "topTransition notShowInput"
                : "topTransition showInput"
            }
            style={{
              position: "absolute",
              background: "white",
              height: "100%",
              width: "calc(100% + 2px)",
              right: -1,
              top: 0
            }}
          ></div>
        </div>
      </div>
    );
  }
}
