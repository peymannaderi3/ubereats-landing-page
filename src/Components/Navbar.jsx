import React from "react";
import uberLogo from "../Assets/uberEats.logo.svg";
import Search from "./Search";
export default class Navbar extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <React.Fragment>
        <header
          id="stickyNav"
          style={{
            width: "100%",
            position: "sticky",
            top: 0,
            zIndex: 100,
            background: "white"
          }}
        >
          <div
            className="wrapper"
            style={{
              height: 80,
              margin: "auto",
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center"
            }}
          >
            <a>
              <img src={uberLogo}></img>
            </a>
            <div
              className="notShowTopNav"
              style={{
                width: "calc(100% - 220px)",
                minWidth: 100
              }}
            >
              <Search bgColor="#F5F5F5" width="80%" id="topInput"></Search>
            </div>
            <a
              style={{
                lineHeight: "24px",
                fontSize: 16,
                color: "#1F1F1",
                textDecoration: "none"
              }}
            >
              Sign In
            </a>
          </div>
        </header>
        <header
          id="secondNav"
          style={{
            width: "calc(100%-35px)",
            position: "sticky",
            top: 0,
            zIndex: 100,
            padding: 16,
            background: "white",
            display: "none",
            boxShadow: "rgba(0, 0, 0, 0.1) 0px 0px 10px"
          }}
        >
          <input
            style={{
              border: 0,
              background: "#F5F5F5",
              flex: 1,
              fontSize: 16,
              width: "85%",
              height: 48,
              padding: 0,
              paddingLeft: 40,
              position: "relative"
            }}
            placeholder="Enter delivery address"
          ></input>
        </header>
      </React.Fragment>
    );
  }
}
