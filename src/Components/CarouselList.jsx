import React from "react";
//asset
import rightArrow from "../Assets/right-arrow.svg";
import leftArrow from "../Assets/left-arrow.svg";

import menuList from "../menu.json";

export default class CarouselList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      carouselPage: 0,
      mobile: false,
      items: 3
    };
  }
  fixSize = () => {
    var items = 3;
    if (window.innerWidth > 600)
      document.getElementById("secondNav").classList.remove("showSecondNav");
    if (window.innerWidth <= 768) items = 2;
    if (this.state.items != items) this.setState({ items, carouselPage: 0 });
    if (window.innerWidth <= 570 && !this.state.mobile)
      this.setState({ mobile: true, carouselPage: 0 });
    else if (window.innerWidth > 570 && this.state.mobile) {
      //document.getElementById("carouselParent1").scroll(0, 0);
      document.getElementById("carouselParent1").scrollLeft = 0;
      this.setState({ mobile: false, carouselPage: 0 });
    }
  };
  componentDidMount() {
    this.fixSize();
    window.addEventListener("resize", this.fixSize);
  }
  componentWillUnmount() {
    window.removeEventListener("resize");
  }

  render() {
    const { title } = this.props;
    return (
      <div style={{ width: "100%" }}>
        <div
          style={{
            width: "100%",
            marginBottom: 24,
            position: "relative"
          }}
        >
          <p
            style={{
              fontSize: 26,
              lineHeight: "32px",
              position: "inline-block",
              textAlign: "left",
              marginRight: this.state.mobile ? 0 : 90
            }}
          >
            {title}
          </p>

          <div
            className="carouselButton"
            style={{
              width: 88,
              height: 40,
              position: "absolute",
              right: 0,
              top: 0,
              display: "flex",
              justifyContent: "space-evenly"
            }}
          >
            <button
              style={{
                height: 40,
                background: "#F5F5F5",
                width: 40,
                cursor: "pointer",
                justifyContent: "center",
                alignItems: "center",
                display: "flex",
                opacity: this.state.carouselPage < 1 ? 0.3 : 1
              }}
              disabled={this.state.carouselPage < 1}
              onClick={() => {
                this.setState({
                  carouselPage: this.state.carouselPage - 1
                });
              }}
            >
              <img src={leftArrow} style={{ width: 18 }}></img>
            </button>
            <button
              style={{
                height: 40,
                background: "#F5F5F5",
                width: 40,
                cursor: "pointer",
                justifyContent: "center",
                alignItems: "center",
                display: "flex",
                opacity:
                  menuList.length / this.state.items <=
                  this.state.carouselPage + 1
                    ? 0.3
                    : 1
              }}
              onClick={() => {
                if (
                  menuList.length / this.state.items >
                  this.state.carouselPage + 1
                )
                  this.setState({
                    carouselPage: this.state.carouselPage + 1
                  });
              }}
            >
              <img src={rightArrow} style={{ width: 18 }}></img>
            </button>
          </div>
        </div>
        <div
          id="carouselParent1"
          style={{
            width: "100%",
            display: "block",
            overflow: "hidden"
          }}
          className="carouselParent"
        >
          <div
            className="carousel"
            style={{
              display: "flex",
              marginBottom: 10,
              flexWrap: "nowrap",
              transform: `translateX(${-this.state.carouselPage *
                100}%) translateX(2px)`
            }}
          >
            {menuList.map((item, i) => {
              return (
                <div
                  key={"carousel-" + i}
                  className="cardSize"
                  style={{
                    flexBasis: "calc(33.333% - 10px)",
                    flexShrink: 0,
                    marginRight: 10
                  }}
                >
                  <a
                    href=""
                    style={{ textDecoration: "none", color: "#1F1F1F" }}
                  >
                    <img
                      style={{ height: 240, width: "100%" }}
                      src={item.image}
                    ></img>
                    <div style={{ textAlign: "left" }}>
                      <p
                        className="nomg"
                        style={{ marginTop: 5, fontSize: 16 }}
                      >
                        {item.title}
                      </p>
                      <p
                        className="nomg"
                        style={{
                          marginTop: 5,
                          color: "#75757",
                          fontSize: 14
                        }}
                      >
                        ${item.price} • {item.cuisineType.title} •{" "}
                        {item.mealType.title} •{item.menuType.title}
                      </p>
                      <div
                        style={{
                          width: "100%",
                          padding: 5,
                          display: "flex",
                          flexWrap: "wrap"
                        }}
                      >
                        <div
                          style={{
                            background: "#F5F5F5",
                            padding: "2px 8px",
                            marginTop: 8,
                            display: "inline-block",
                            fontSize: 14,
                            color: "#1F1F1F",
                            marginRight: 5
                          }}
                        >
                          20-30 min
                        </div>
                        <div
                          style={{
                            background: "#F5F5F5",
                            padding: "2px 8px",
                            marginTop: 8,
                            display: "inline-block",
                            fontSize: 14,
                            color: "#1F1F1F",
                            marginRight: 5
                          }}
                        >
                          4.3 * (+200)
                        </div>
                        <div
                          style={{
                            background: "#F5F5F5",
                            padding: "2px 8px",
                            marginTop: 8,
                            display: "inline-block",
                            fontSize: 14,
                            color: "#1F1F1F",
                            marginRight: 5
                          }}
                        >
                          $2.49 Delivery Fee
                        </div>
                      </div>
                    </div>
                  </a>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}
